# nintendo wii 

## letterbomb
* https://please.hackmii.com/ - letterbomb online generator
* https://www.makeuseof.com/tag/set-wii-homebrew-letterbomb/

## wiibrew projects
* http://wiibrew.org/wiki/Disk_Drive_Lighter
* http://wiibrew.org/wiki/GeeXboX
* http://wiibrew.org/wiki/MPlayer_CE
* http://wiibrew.org/wiki/WiiMC
* http://wiibrew.org/wiki/Schism_Tracker
* http://wiibrew.org/wiki/WiiRadio
* http://wiibrew.org/wiki/ASCII_Paint
* http://wiibrew.org/wiki/Balance_Board_Pro
* http://wiibrew.org/wiki/Balance_Board_Tools
* http://wiibrew.org/wiki/BootMii_Configuration_Editor
* http://wiibrew.org/wiki/BootOpera
* http://wiibrew.org/wiki/DOP-Mii:_WiiBrew_Edition
* http://wiibrew.org/wiki/Epilepsii
* http://wiibrew.org/wiki/Ftpii
* http://wiibrew.org/wiki/Homebrew_Browser
* http://wiibrew.org/wiki/Open_Shop_Channel
* http://wiibrew.org/wiki/RSSMii
* http://wiibrew.org/wiki/SD_Explorer
* http://wiibrew.org/wiki/WiiEarth
* http://wiibrew.org/wiki/Wii_FlashToolz
* http://wiibrew.org/wiki/WiiStrobe
* http://wiibrew.org/wiki/WiiVNC
* http://wiibrew.org/wiki/WiiXplorer
* http://wiibrew.org/wiki/Boot_it
* http://wiibrew.org/wiki/WiiShell

## (d2x-) cios
* https://wii.guide/cios.html
* https://sourceforge.net/projects/d2x-cios/
* https://wiidatabase.de/downloads/hacks/d2x-cios/
* https://code.google.com/archive/p/d2x-cios/wikis/How_To_Install.wiki

## wiimote
* https://invisibleagent.com/blog/news/virtual-painting-wall-wii-remote-at-tedx-phnom-penh-recycled-equipment

## wii fit balance board
* https://djmojodojo.wordpress.com/2009/12/05/airdeck-it-lives/

## other projects
* http://johnnylee.net/projects/wii/

## on gitlab
* https://gitlab.com/growingstems/Wii-Bot
* https://gitlab.com/1480c1/friidump

