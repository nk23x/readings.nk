# [readings.nk](https://gitlab.com/nk23x/readings.nk/-/tree/master)

[Texts and Documents](https://gitlab.com/nk23x/readings.nk/-/tree/master)

## toolkit
* HTML Cleaners
  * https://html-cleaner.com/
* HTML to MD
  * https://www.browserling.com/tools/html-to-markdown
  * https://pandoc.org/try/
  * https://domchristie.github.io/turndown/