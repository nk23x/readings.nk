```
Freie Deutsche Jugend, Bundesvorstand, Karl-Liebknecht-Haus,
Weydingerstr. 14-16, 10178 Berlin
source url: https://www.fdj.de/pdf/1990.pdf
```
# Die DDR-Volkskammerwahlen 1990 und die Einmischung der BRD

Die 10.Volkskammerwahl 1990 sollte dem Volk in der DDR zeigen, was die BRD unter sogenannten
„freien Wahlen“ versteht.

Der britische Journalist C.C.O’Brian ahnte schon, was mit der "Wiedervereinigung“ kommen sollte und
schrieb am 31.Oktober 89 in der Zeitung „Times“ u.a.:

„... Die Auflösung des sowjetischen Imperiums und die deutsche Widervereinigung. Beides zusammen
werden in den Beginn einer deutschen Wirtschaftshegemonie münden, die von den Aran-Inseln vor
der Westküste Irlands bis nach Wladiwostok reicht. Briten, Franzosen, Europäer allgemein fühlen sich
mit Recht tief beunruhigt bei dieser Aussicht.(...) Aber weder die Westeuropäer noch die
Vorsichtigeren unter den Deutschen können die Wiedervereinigung abwenden. Die einzigen, die dazu
vielleicht in der Lage wären, sind die sowjetischen Machthaber. Verhindern können sie es, indem sie
Panzer nach Ostdeutschland schicken. Die nicht-interventionistische Politik scheint dies
auszuschließen.“ (zit. nach Blätter für deutsche und internationale Politik 12/89)

Die direkte Einmischung der BRD-Parteien auf die Parteien der DDR begann nicht erst mit dem Fall
der Mauer. Als sich im Herbst 1989 die Ereignisse in der DDR überschlugen, trieben sich die Parteien
der BRD in der DDR herum. So bedrängte die FDP in dieser Zeit die LDPD zur Zusammenarbeit, bis
sie in ihrer Plattform die wichtigsten Punkte aus dem FDP-Programm übernommen hatte. 

Später, im Januar 1990 zum Vereinigungsparteitag von FDP und Bund Freier Demokraten (BFD), ein Bündnis
aus LDPD und einem Teil der NDPD, das nach dem 9.November 1989 gebildet wurde), hatte die BFD
doppelt so viele Mitglieder wie die FDP. Damit die FDP mit ihrer Delegierten auf dem Parteitag nicht in
eine Minderheit gerät, veränderte sie einfach die Grundlage zur Berechnung des
Delegiertenschlüssels so, dass nicht die Mitgliederzahlen, sondern die Wahlstimmen die
Ausgangsbasis zur Berechnung war. Dies kehrte das Delegiertenverhältnis zugunsten der FDP um. In
der Folgezeit wurden von den ehemaligen 4100 Hauptamtlichen der NDPD und der LDPD 3300 ohne
die versprochenen Pensionszahlungen entlassen.

Die SPD ging etwas anders vor: Sie ließ die Sozialdemokratische Partei (SDP) gründen – provokativ
zum 40.Jahrestag der DDR-Gründung. Kaum war die Mauer gefallen, sprang Egon Bahr auf die
Bühne und forderte in aggressive Weise von der SED die 75 Mio. Mark zurück, die die SPD bei der
Vereinigung mit der KPD behalten hatte.

Als im Dezember 1989 die angeblich von der SPD unabhängige SDP ein Wahlbündnis mit DDR-
Parteien schließen wollte, da untersagte ihr dies die SPD aus der BRD. Auf dem Parteitag am
13.Januar 1990 nahm die SDP entsprechend den Namen SPD an und erklärte als Hauptziel: Kampf
gegen PDS/SED und propagierte den Anschlu0 der DDR an die BRD nach Artikel 146 GG.
Die CDU und CSU hielten sich im Herbst 89 mit ihrem Parteiaufbau noch relativ zurück, gingen aber
um so schärfer ab Januar 1990 vor.

Alle im Bundestag vertretende Parteien waren sich daran einig: Die DDR muss beseitigt, sprich
annektiert werden. Dazu war ein Sieg bei den Volkskammerwahlen von propagandistischer
Bedeutung.

Hans Modrow wurde in einer geheimen Sitzung der Volkskammer am 13.November 1989 zum
Ministerpräsidenten der DDR gewählt. Vier Tage später hielt er eine Regierungsansprache, in der er
eine kooperative Vertragsgemeinschaft mit der BRD forderte. Aber weder diesen Vorschlag noch die
Forderung nach einer BRD-Finanzhilfe von 15 Mrd. DM zur Unterstützung der DDR-Ökonomie im
Februar 1990 wurde von der Kohl-Regierung angenommen. 

Modrow hatte nach dem Willen der BRD-Regierung zu tanzen. Hätte er es nicht getan, wäre er sicherlich 
nicht lange in seinem Amt geblieben.

Dies betraf nicht nur Modrow, sondern alle maßgeblichen Politiker in der DDR. Sie wurden von den
Annexionisten benutzt und unter Druck gesetzt, so wie sie es brauchten. So auch die
Bürgerbewegungen in der DDR („Neues Forum“, „Demokratie jetzt“, Demokratischer Aufbruch“,
„Vereinigte Linke“ u.a.m.), die sich im Herbst herausbildeten. Ebenso blieb der „Runde Tisch“ bis zu
seiner Auflösung am 12.März für die BRD lediglich ein Mittel zum Zweck.. 

Das zeigten spätestens die Wahlergebnisse zur Volkskammer am 18.März, wo sie kaum noch eine Rolle spielten.

Am 3.Januar 1990 demonstrierten 250 000 Menschen am Treptower Park gegen den von der BRD
organisierten, faschistischen Mob und gegen die Regierung in der DDR, die nichts dagegen
unternahm. Von der bürgerlichen Presse wurde der faschistische Terror als hochgespielte SED-
Propaganda dargestellt. In der gleichen Woche belagerten in München ein Bündnis von Jugendlichen
in München unter Führung der Münchner Initiative zur Vereinigung der revolutionären Jugend an
mehreren Tagen den Zufahrtsweg der CSU-Zentrale. Der Grund war: Die CSU beabsichtigte in die
DDR einzufallen, um ihre alljährliche Klausurtagung von Kreuth/Bayern dieses Jahr in Leipzig
abzuhalten. Die Jugendlichen trugen Schilder mit der Aufschrift „Verweist die CSU in die Grenzen von

1949“ und „Stoppt die Geburtshelfer des IV. Reiches“. Das Ganze wurde musikalisch unterstützt. Die
Jugendlichen blieben solange sitzen, bis die Polizei sie wegtrug. Dies war eine wichtige konkrete
Aktion in der BRD gegen den Einfall der westdeutschen Bourgeoisie in die DDR.

## Der „Runde Tisch“

Die Gründung des Runden Tisches war gedacht als ein gemeinsames Forum von „Opposition und
DDR-Volkskammer“ zur Lösung der anstehenden Fragen. Faktisch war er jedoch gegen die DDR-
Volkskammer gegründet worden.

Der Runde Tisch setzte sich aus Vertretern von 12 verschiedenen DDR-Organisationen und –Parteien
zusammen und stand unter der Leitung der evangelischen Kirche. Mit 3 Vertretern moderierten sie die
Sitzungen. In der Sitzordnung saßen sich „Oppositionsvertreter“ und Volkskammervertreter
gegenüber. Die SED musste sich nach wochenlangem Streit einen Sitz am Runden Tisch erkämpfen.

Der Runde Tisch trat zwischen dem 7.Dezember und dem 12.März 16mal zusammen. 

Er war hauptsächlich mit Verfassungsänderungen beschäftigt, die zur Liquidierung des Sozialismus und der
Staatssicherheit in der DDR führten. Mit jedem Treffen nahm die Machtbefugnis des Runden Tisches
zu. Seit Mitte Januar konnte keine politische Entscheidung der Volkskammer ohne Votum des Runden
Tisches verabschiedet werden. Einen DDR-Verfassungsentwurf und mehr als 100 Gesetzesentwürfe
wurden ausgearbeitet. So beschloss der Runde Tisch am 12.Januar auch ein Gesetz, das
ausländischen Kapitalisten erlaubte, mehr als 50% von einem DDR-Betrieb zu erwerben.

## ... und was war mit der FDJ?

Die FDJ erhielt nur einen Beobachterstatus am Runden Tisch. Aus diesem Grund bildete die FDJ
einen Runden Tisch der Jugend mit anfänglich 18, später 22 neu gegründeten Jugendverbänden und
eine „Alternative Jugendliste“ für die Volkskammerwahl, bestehend aus drei Jugendorganisationen.
Ihm fehlten nur 484 Stimmen für einen Sitz in der Volkskammer.

## Zu den Volkskammerwahlen am 18.März 1990

Schon am 15.Januar bot Modrow dem Runden Tisch eine Regierungsbeteiligung an. Die
Volkskammer entschied am 28.Januar, die Wahlen vom 6.Mai auf den 18.März 1990 vorzuverlegen.
Dafür trat besonders die SPD ein – aber auch die PDS, da sie sich einen Wahlsieg erhofften.

Am nächsten Tag schlug Kanzler Kohl im CDU-Präsidium ein Wahlbündnis mit dem Namen „Allianz
für Deutschland“ (mit CDU, DA, DSU) vor. Zum 1.Februar 1990 lud Kohl diese DDR-Parteien in sein
Berliner Domizil ein. Das Wahlbündnis wurde am 5.Februar gegründet und trat unter dem Motto
„Freiheit und Wohlstand – Nie wieder Sozialismus“ in den Wahlkampf. Kohl versprach in der DDR: in
3-5 Jahren werden die neuen Bundesländer blühende Landschaften sein.

Als Modrow Ende Januar von Gorbatschow grünes Licht für die Einheit beider deutscher Staaten
erhielt, machte er den sogenannten „Modrow-Plan“ öffentlich: gemeinsame Regierung, Übergabe der
Souveränitätsrechte beider Staaten, gemeinsame Verfassung.

Ohne Rücksprache mit der Volkskammer bildete Modrow am 5.Februar ein neues Kabinett der
„Regierung der nationalen Verantwortung“. Die Volkskammer entschied zusätzlich acht Minister ohne
Geschäftsbereich einzustellen, die die wichtigsten Oppositionsgruppen präsentierten. Ohne sie sei ein
Weiterregieren nicht möglich – so Modrow. Damit erhielten die Oppositionsgruppen auch noch die
Mehrheit der Stimmen in der Regierung.

Zwei Tage später beschloss die BRD-Regierung einen Ausschuss „Deutsche Einheit“ unter dem
Vorsitz von Helmut Kohl einzurichten. In ihm waren 8 Bundesminister ständige Mitglieder. Zur
Anpassung der wichtigsten Bereiche (Finanzen, Wirtschaft, Arbeit und Soziales) wurden 6
Arbeitsgruppen eingerichtet. Schäuble installierte einen eigenen Arbeitsstab mit 20 Mitarbeiter. Dieser
sollte sich später als Keimzelle für den Einheitsvertrag herausstellen. Die Aufgabe des Arbeitsstabs
war die rechtliche Absicherung der Annexion, dessen Zeitpunkt und Form noch nicht absehbar war.
Klar war: Der Anschluss nach Artikel 23 GG muss vollzogen werden – egal wie, so Schäubles
Direktive.

Die DSU hatte ihren Gründungsparteitag Mitte Februar in der Oper von Leipzig. Es gab 2 Linien in der
DSU-Führung. Die eine war nur auf die CSU ausgerichtet, die andere Linie war repräsentiert durch
Ebeling. Er war gegen die alleinige Ausrichtung auf die CSU. Ebeling wurde massiv von der CDU
unterstützt und bekam von ihr sogar ein Auto gestellt. Auf diesem DSU-Parteitag war fast die
komplette CSU-Führung anwesend. Der Saal war blau-weiß geschmückt wie auf einem CSU-
Parteitag. Die Tagesunterlagen waren von der CSU signiert. Aber auch CDU-Spitzen wie Wolfgang
Schäuble waren auf dem Parteitag vertreten. Zum Schluss wurde die BRD-Nationalhymne gesungen.

Die Volkskammerwahl war von der BRD finanziert

Der Runde Tisch hatte es im Januar 1990 untersagt, dass sich Parteien und deren Redner aus der
BRD in den DDR-Wahlkampf einmischen. Dies war die herrschende Haltung in der DDR-Bevölkerung.

Selbst von den SPD-Anhängern in der DDR befürworteten nur 35% diese Einmischung aus der BRD.
Den Annexionisten interessierte dies aber überhaupt nicht. Verschiedene Quellen geben eine BRD-
Finanzierung der DDR-Volkskammerwahl von 20 Mio. DM an. Mehr als 200 Wahlkampfreden wurden
von Westparteien in der DDR gehalten.

Die SPD ließ sogar im Januar 1990 in der DDR-Hauptstadt Berlin eine Filiale der staatseigenen
Westdeutschen Landesbank für die Versorgung der Wahlkämpfe in der DDR einrichten (insbesondere
für den Landtagswahlkampf in Brandenburg). Die WestLB hatte damit die stärkste Stellung der BRD-
Banken in Berlin. Zudem ist es vollkommen rechtswidrig, dass von Staatsgeldern direkt Parteien
finanziert werden. Allein das Parteibüro kostete 1990 1,2 Mio. DM. Lange vor der Landtagswahl hatte
die SPD in Nordrhein-Westfalen festgelegt, wer welchen Ministerposten in der Landesregierung
Brandenburgs erhalten soll. 

Auch bekam jedes Ministerium einen Ansprechpartner aus der BRD 
zugewiesen (aus Der Spiegel, 7/00). Nachdem also die CSU aus 12 Splitterparteien die DSU gebildet
hatte, übernahm diese weitgehend das CSU-Programm, was ihr die CSU dann auch in München
druckte. Die CSU rühmte die DSU als ihre „Braut“. Sie schenkte ihr u.a. Schreibmaschinen und die
Wahlausgabe „Kurier der Deutschen Sozialen Union“ in 4 Mio. Exemplaren, 2 Mio. Flugblätter, 1 Mio.
DSU-Grundsatzprogramme, 1 Mio. Grundsatzprogramme. Insgesamt wurden von der CSU 25 Tonnen
Propagandamaterial in die DDR rübergekarrt. Über 4 Wochen lang entsandte sie alle ihre 12
hauptamtlichen Wahlkreisgeschäftsführer.

## Die CDU-Wahlkampfhilfe

Am 9.Januar 1990 forderte CDU-Generalsekretär Rühe die CDU in der DDR auf, die bis dahin
gemeinsam getragene Regierung mit Modrow zu verlassen. In der TV-Serie „Chronik der Wende“
sagte er als Reaktion auf den Beschluss des Runden Tisches sinngemäß: Wenn wir ihnen schon die
DM geben, kann es nicht angehen, dass sie uns nicht rein lassen. Rühe forderte jeden CDU-
Kreisverband auf, sich einen Partnerkreis in der DDR auszusuchen, um Wahlkampfhilfe für die „Allianz
für Deutschland“ zu betreiben. Der damalige Regierungssprecher Friedhelm Ost leitete die
entsprechende Logistik in Berlin/DDR. Die CDU mobilisierte 50 Wahlredner für den DDR-Wahlkampf
(darunter auch Bundeskanzler Kohl).

7.5 Millionen DM steckte das Bundesministerium für Innerdeutsche Beziehungen über parteinahe
Stiftungen von CDU, CSU, FDP und SPD in den DDR-Wahlkampf. 2,5 Tonnen Kleister zum Anbringen
der Wahlplakate wurden direkt von der Firma Henkel zur Verfügung gestellt und der Demokratische
Aufbruch bekam eine komplette Druckmaschine von der CDU hingestellt.

Faschistische Parteien wurden zur Wahl nicht zugelassen. Anfang Februar verbot die Volkskammer
das Auftreten der Republikaner in der DDR. Trotzdem verteilten diese nach eigenen Angaben 5
Tonnen faschistisches Material in der DDR. An der DDR-Grenze behaupteten sie, sie würden die
Transitstrecke nach Westberlin fahren.

## ... und die Medien
Es war unerlässlich für die ganze Lügen- und Hetzpropaganda in der DDR, dass die DDR-Presse in
der Hand der Annexionisten gerät. 

Vier Medienkonzerne teilten sich die DDR unter sich auf:
- Bauer-Verlag : Mecklenburg-Vorpommern und Brandenburg
- Gruner+Jahr: Großraum Berlin
- Springer und Burda: Sachsen und Thüringen

Am 23.1.90 schlossen diese Konzerne mit DDR-Verlagen einen entsprechenden Geheimvertrag. Die
Volkskammer erklärte diesen Vertrag für ungültig. Aber spätestens ab Anfang März stürmten diese
Verlage aus der BRD den DDR-Markt.

In den beiden ersten Märzwochen vor der Wahl fanden von den verschiedenen politischen Richtungen
598 Kundgebungen und Demonstrationen mit insgesamt 1,2 Mio. Teilnehmern statt. Der Wahlkampf
war geprägt von der Propagierung des schnellen Anschlusses und die Diskriminierung der PDS und
aller derjenigen, die eine eigenständige DDR erhalten wollten. Die CDU hatte die Losung „Wohlstand
statt Sozialismus – nie wieder Sozialismus“ und die SPD die Losung: „Die Zukunft hat wieder einen
Namen. SPD“ sowie „Wohlstand, Freiheit, Einheit“. 

Die DSU setzte im Wahlkampf Schlägerbanden
gegen die Konkurrenz ein. So wurden z.b. einem SPD-Wahlhelfer beide Beine gebrochen. Sie trat
auch nach der Wahl mit faschistischer Agitation und für die Liquidierung der DDR-Parteien auf. 

Fasste
der Runde Tisch am 20.Februar noch den eindeutigen Beschluss gegen eine NATO-Mitgliedschaft der
DDR, für ein entmilitarisiertes Deutschland und gegen einen Anschluss der DDR nach Artikel 23 GG,
schwenkten die meisten DDR-Parteien noch vor den Wahlen um. 

Bis zuletzt standen die Prognosen
bei einem eindeutigen Sieg der SPD. Das ergaben alle offiziellen Umfragen in der DDR (aus: Die
Zeit). Im Januar sprachen sich 54% der Wähler für die DDR-SPD aus. Dass jedoch das reaktionäre
Wahlbündnis „Allianz für Deutschland“ die Volkskammerwahl zur Überraschung aller gewann, lässt
darauf schließen, mit welchem Terror und Diffamierungen sie gegen jene vorgegangen sein müssen,
die die DDR als eigenständigen Staat erhalten wollten. Einen Eindruck davon gibt am Wahlabend
Egon Bahr (SPD): „Was ich in dieser Zeit in der DDR gesehen habe, hat mich zutiefst empört. 

Die
gesamte Wahlkampagne wurde zu einer von der westdeutschen CDU/CSU gesteuerten Aktion
gemacht. Das war eine unwürdige Aktion, auf dies sich keine einzige seriöse politische Partei der BRD
eingelassen hätte. Bezahlte Gruppen von Jugendlichen entfernten und überklebten Wahlplakate der
SPD. Es ging sogar so weit, dass Lautsprecherwagen mit Münchner Nummer, wo bekanntlich die
CSU ihren Sitz hat, die Leipziger Straßen überfluteten und über Megaphone aufforderten, nicht zu den
Kundgebungen mit Willi Brandt zu gehen... 

In kleineren Städten in Thüringen und Sachsen wurden
vielen bekannten Mitgliedern der SPD und der PDS heimlich Drohbriefe bis hin zu physischer
Abrechnung zugestellt... Auch Kinder mussten herhalten. Man gab ihnen Westgeld, damit sie durch
die Höfe laufen und Flugblätter der Deutschen Sozialen Union, der Tochterpartei der westdeutschen
CSU verteilten. Das war reinster psychischer Terror nach Göbbels Manier. Ich möchte wiederholen,
dass dieser politischer Schmutz aus der BRD exportiert wurde.“ (aus Fanfare, Zeitung der FDJ, Nr.23, S.2).

## Wahlergebnis Volkskammerwahl
24 Parteien kandidierten zur Wahl am 18.März. Keine der DDR-Bürgerrechtsparteien erreichte mehr
als 3%. Die „Allianz“ hatte im Schnitt 46% erreicht, in den Städten 26,5%, auf dem Land über 60% der
Wählerstimmen. Die PDS gewann trotz des Terrors über 16% der Stimmen.

Die 10.Volkskammer hatte eine vollkommen veränderte soziale Zusammensetzung der Abgeordneten:
Die Akademiker hatten insgesamt einen Anteil von 85%, bei SPD und PDS sogar um 90%. Nur 2%
der Abgeordnete waren Arbeiter. 7 von 8 waren bei reaktionären und faschistischen Parteien
organisiert. Die Wahlbeteiligung lag bei 93,3%. Zur ersten großdeutschen Wahl im Dezember 1990
sollten es schon 20% weniger sein.

Nach den geringen Wahlerfolgen von DA und DSU traten diese noch im gleichen Jahr ihren
Mutterparteien bei. Später äußerte Schäuble: Ich hätte es für geschickter gehalten, wenn die CSU
direkt in der DDR kandidiert hätte.

Am 12.4. 90 wurde die Koalitionsvereinbarung von CDU, DSU, DA, Liberale und SPD zum Beitritt
nach Artikel 23 GG geschlossen. Die Annexionsparteien und ihre Vasallen schließen sich zusammen
und regieren ein fremdes Land nach preußischer Manier. Trat die SPD noch vor der Wahl für den
Artikel 146 GG ein, so war das aus rein taktischen Gründen. Auch lehnte die SPD vor der Wahl eine
Koalition mit der DSU ab. Dies spielte aber in den Koalitionsverhandlungen nach der Wahl keine Rolle
mehr.

Die Erklärung des sowjetischen Außenministeriums vom 14.März 90 zum Beginn der 2+4-
Verhandlungen, warnte vor der Kriegsgefahr, die von der Annexion der DDR nach Anschluss Artikel
23 GG ausgeht. Darin heißt es u.a.: „... Von welcher Seite man die Anwendung des Artikel 23 auch
betrachten möge, wäre ein solches Vorgehen unrechtmäßig und unakzeptabel... Man darf nicht
vergessen, dass die Sowjetunion, Frankreich, Großbritannien und die USA laut Potsdamer Abkommen
ihre Rechte und die Verantwortung für Deutschland als Ganzes und dafür, dass von deutschem
Boden nie mehr Gefahr für den Frieden ausgeht, nach wie vor bewahren..... 

Nicht Einverleibung eines
deutschen Staates durch den anderen, nicht voreilige Handlungen, sondern vereinte Anstrengungen
aller interessierten Seiten können eine Regelung der deutschen Frage garantieren... Die Lehren des
Zweiten Weltkriegs verpflichten alle, die von dieser Tragödie betroffen wurden, alles in ihren Kräften
stehende zu tun, damit sie sich künftig nie wiederhole (aus: Blätter für deutsche und internationale
Politik, Heft 4/90, S.505ff).

Die Regierungserklärung des neuen Ministerpräsidenten de Maizière vom 19.April wurde von
Kanzlerberater Teltschik im wesentlichen bestimmt. Besonders begrüßte er die Nichtfestlegung des
Verfahrens zur deutsch-polnischen Grenze. Auch in der Nachkorrektur kam man der Frage der NATO-
Mitgliedschaft im Interesse der BRD entgegen. „Im Großen und Ganzen können wir mit dieser
Regierungserklärung sehr zufrieden sein.“(Teltschik).

Das DDR-Innenministerium erhielt den ehemaligen Präsidenten des BKA, Heinrich Boge als Berater.
Die DSU setzte am 31.5.90 einen Beschluss in der Volkskammer durch, dass alle DDR-Staatswappen
von öffentlichen Gebäuden innerhalb einer Woche zu entfernen sind. Wo dies nicht möglich ist, sollen
sie verdeckt werden (z.b. durch Bettlaken). Dies stellt einen eklatanten Bruch mit der gültigen DDR-
Verfassung und sein Artikel 1 dar. Gleichzeitig verblieb die DDR-Flagge auf dem Dach des Palasts der
Republik. Auch jeder Abgeordnete lief weiterhin mit dem Ährenkranz im Ausweis wie
selbstverständlich herum.

Dass die Annexionsparteien in der DDR nicht so ankamen wie sie glaubhaft machen wollten, zeigt
ihren relativ schnellen Mitgliederschwund. Im Februar 1990 hatte die SPD 100 000 Mitglieder in der
DDR – im Oktober 1990 waren sie wieder auf 20 000 Mitglieder zusammengeschrumpft. Die FDP
hatte im Februar 200 000 Mitglieder, 1995 nur noch 25 000 Mitglieder.


```
Freie Deutsche Jugend, Bundesvorstand, Karl-Liebknecht-Haus,
Weydingerstr. 14-16, 10178 Berlin
```
## Abgeordnete der Volkskammer im Vergleich zu Abgeordnete im BRD-Bundestag
Die Abgeordneten der Volkskammer der DDR wurden alle direkt gewählt und sind laut Verfassung
verpflichtet, regelmäßig Sprechstunden und Aussprachen durchzuführen sowie den Wählern über ihre
Tätigkeit Rechenschaft abzulegen. Ein Abgeordneter, der seine Pflichten gröblich verletzt, kann von
den Wählern abberufen werden. Jeder Abgeordnete der Volkskammer kann Anfragen an den
Ministerrat und an jedes seiner Mitglieder richten.

Im Vergleich dazu das BRD-Grundgesetz: Artikel 38 Satz 1: Sie (die Abgeordneten des Bundestags)
sind Vertreter des ganzen Volkes, an Aufträge und Weisungen nicht gebunden und nur ihrem
Gewisse unterworfen.

Mehr als die Hälfte der Abgeordneten in der Volkskammer waren Arbeiter. Im Vergleich zur BRD: Nur
2,1% (10 Abgeordnete) sind als Arbeiter ausgewiesen.

Für die Volkskammerabgeordneten galt die Ehrenamtlichkeit als Grundsatz. Dies ist eine Anlehnung
an die Erfahrungen der Pariser Kommune. Sie bleiben durch die normale Berufstätigkeit mit der
Bevölkerung verbunden (->Volksvertretung). Dies war im Frühjahr 1990 von einem auf den anderen
Tag vorbei. Die Abgeordneten der 10.Volkskammer führten ihre Tätigkeit hauptamtlich aus. Dies
veränderte die Parlamentskultur grundsätzlich. 

Eine Trennung von Volk und Parlament war damit
vollzogen. Das Parlament war mehrfach Ziel von Demonstrationen und Delegationen. Sie konnten
noch offen ihre Probleme und Forderungen in die Plenarsitzungen einbringen und sie fanden Gehör.
Die PDS lehnte den Fraktionszwang ab. Das zeigte sich in den Abstimmungen, bei denen die PDS-
Abgeordnete oft sehr unterschiedlich abstimmten. Fast alle Abgeordnete der neu gewählten
Volkskammer waren neu. Es war praktisch ein Laienparlament. Aber auf den Plenarsitzungen früher
hörte man den anderen zu und ging aufeinander ein. Die Volkskammer war immer gut besetzt. Stets
wurden immer reale Abstimmungsmehrheiten ausgezählt.

## Zum Umgang mit der DDR-Verfassung
Die sozialistische DDR-Verfassung wurde mit der Installierung des Runden Tisches kontinuierlich
demontiert. Am 12.1.1990 wurde das Recht auf Privateigentum verankert. Tags darauf traf
Wirtschaftsministerin Luft mit 70 Vertretern des BRD-Finanzkapitals zusammen. Der Runde Tisch
arbeitete emsig einen Verfassungsentwurf aus. Er war schon sehr am Grundgesetz der BRD
angelehnt und sollte von der neuen DDR-Regierung beraten werden. 

DDR-Justizminister Wünsche 
lehnte aber den Verfassungsentwurf im Mai 1990 eiskalt mit der Begründung ab, es müssten noch die
Artikel raus, die dem Staatsvertrag im Wege stehen. Die Volkskammer schloss sich bald darauf dem
Justizminister an. Daraufhin arbeitete eine „Expertenkommission“ ein „vorläufiges Grundgesetz der
DDR“ aus. Den drei anwesenden Vertretern aus dem Bonner Ministerium war selbst dieser Entwurf
noch zu demokratisch und diktierten klar, was da nicht reingehört. Sie liquidierten die letzten Reste der
DDR-Verfassung. 

Am 17.Juni 1990 kam es zur Verabschiedung der sogenannten
„Verfassungsgrundsätze“, bestehend aus 10 Artikeln, in denen explizit sozialistische Eigentumsformen
aufgehoben und durch das Privateigentum an Boden und Produktionsmitteln ersetzt wurde. Dieser
war nur für die Übergangszeit bis zum Anschluss nach Artikel 23 GG gedacht. Eine Umfrage in der
DDR am 10.April 1990 ergab, dass nur 9% für die Übernahme des BRD-Grundgesetzes waren, 38%
für eine gemeinsame Verfassung und 42% für eine eigene DDR-Verfassung.

veröffentlicht durch

Freie Deutsche Jugend,
Bundesvorstand,
Karl-Liebknecht-Haus
Weydingerstr. 14-16, 10178 Berlin,
Tel. 030/24 00 92 11, Fax 030/28 38 52 80

E-Mail: kontakt@FDJ.de
Homepage: [http://www.FDJ.de](http://www.FDJ.de)


